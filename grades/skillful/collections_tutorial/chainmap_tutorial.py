"""
ChainMap - это последовательность словарей.
При запросе ключа, будет произведён поиск 
по всем словарям внутри ChainMap в порядке
объявленом при его инициализации.
"""
from collections import ChainMap

one_dict = {"a": "a", "b": "b"}
two_dict = {1: 1, 2: 2}
three_dict = {
    "a": "another_value", 
    1: "number", 
    "unique_key": "this key is unique"
}
chainmap = ChainMap(one_dict, two_dict, three_dict)
print(f"{chainmap = }\n")
print(f"{chainmap['a'] = }")            # будет взят из one_dict
print(f"{chainmap[1] = }")              # будет взят из two_dict
print(f"{chainmap['unique_key'] = }")   # будет взят из three_dict
print()


""" 
Если поменять порядок при инициализации, 
порядок взятия ключей будет другой 
"""
chainmap = ChainMap(three_dict, one_dict, two_dict)
print(f"{chainmap['a'] = }")            # будет взят из three_dict
print(f"{chainmap[1] = }")              # будет взят из three_dict
print(f"{chainmap['unique_key'] = }")   # будет взят из three_dict

"""
defaultdict - это словарь с значением 
по умолчанию для каждого ключа.

Преимущества:
1) Если ключа нет в словаре, то будет возвращено значение по умолчанию
defaultdict(int)   = 0
defaultdict(float) = 0.0
defaultdict(bool)  = False
defaultdict(list)  = []
defaultdict(dict)  = {}
defaultdict(set)   = set()
"""

from collections import defaultdict


numbers = defaultdict(int, {"one": 1, "two": 2, "three": 3})
checklist = defaultdict(bool, {"sql": False, "datatypes": True})
people_skills = defaultdict(list, {"Dmitry": ["SQL", "python", "fastApi"]})

""" Если значения нет, берётся значение по умолчанию """
print(numbers["empty"])         # 0
print(checklist["algorithms"])  # False
print(people_skills["Eugeny"])  # []


""" Очень удобно заполнять словари, у которых в качестве значения списки """
data = [
    ("boss", "Lich King"),
    ("boss", "ragnaros"),
    ("npc", "Leeroy"),
    ("npc", "Sven"),
    ("boss", "Deathwing"),
]

database = defaultdict(list)
for npc_type, name in data:
    database[npc_type].append(name)
# {'boss': ['Lich King', 'ragnaros', 'Deathwing'], 'npc': ['Leeroy', 'Sven']}
print(database)


""" Можно назначать своё значение по умолчанию """
data_hundreds = defaultdict(lambda: 100)
data_hundreds["hundreds"]
data_hundreds["100"]
data_hundreds[100]
# {'hundreds': 100, '100': 100, 100: 100}
print(data_hundreds)

cars_location = defaultdict(lambda: "undefined")
cars_location["bmw 900"] = "Moskow"
cars_location["kia 245"] = "Podolsk"
cars_location["shkoda 832"]
# {'bmw 900': 'Moskow', 'kia 245': 'Podolsk', 'shkoda 832': 'undefined'}
print(cars_location)

numbers = defaultdict(lambda: [1,2,3])
numbers["one_to_four"].append(4)
numbers["default"]
numbers["one_to_two"].pop()
""" 
{'one_to_four': [1, 2, 3, 4], 'default': [1, 2, 3], 'one_to_two': [1, 2]} 
"""
print(numbers)

"""
deque работает как список, но с особенностями:
1) Не поддерживает слайсинг a_deque[0:2]
2) Тред сейфовый
3) Добавление/удаление элементов с обоих сторон одинаково быстрое
4) Добавление/удаление посередине deque медленне чем в list
"""

from collections import deque
from time import perf_counter
import random


TIMES = 1_000


def benchmark(func, times=TIMES):
    total_time = 0.0

    for _ in range(times):
        start = perf_counter()
        func()
        end = perf_counter()
        total_time += end - start

    avg_time = total_time / times
    return avg_time * 1e9


# Удаление элемента слева
a_list = [1] * TIMES
a_deque = deque(a_list)

list_pop_0_time = benchmark(lambda: a_list.pop(0))
deque_pop_left_time = benchmark(lambda: a_deque.popleft())
gain = list_pop_0_time / deque_pop_left_time

print(f"{list_pop_0_time = :0.2f}ns")
print(f"{deque_pop_left_time = :0.2f}ns, {gain = :0.2f}x times\n")
"""
list_pop_0_time = 203.70ns
deque_pop_left_time = 125.20ns, gain = 1.63x times
"""

# Добавление элемента слева
a_list = [1] * TIMES
a_deque = deque(a_list)

list_insert_0_time = benchmark(lambda: a_list.insert(0, 1))
deque_append_left_time = benchmark(lambda: a_deque.appendleft(1))
gain = list_insert_0_time / deque_append_left_time

print(f"{list_insert_0_time = :0.2f}ns")
print(f"{deque_append_left_time = :0.2f}ns, {gain = :0.2f}x times\n")
"""
list_insert_0_time = 590.90ns
deque_append_left_time = 141.90ns, gain = 4.16x times
"""


# Добавление элемента посередине списка
a_list = [1] * TIMES
a_deque = deque(a_list)
middle = TIMES // 2

list_insert_middle_time = benchmark(lambda: a_list.insert(middle, 1))
deque_insert_middle_time = benchmark(lambda: a_deque.insert(middle, 1))
gain = list_insert_middle_time / deque_insert_middle_time

print(f"{list_insert_middle_time = :0.2f}ns")
print(f"{deque_insert_middle_time = :0.2f}ns, {gain = :0.2f}x times\n")
"""
list_insert_middle_time = 423.10ns
deque_insert_middle_time = 698.40ns, gain = 0.61x times
"""


# Добавление элемента в рандомное место списка
a_list = [1] * TIMES
a_deque = deque(a_list)


def random_insert(seq):
    r = random.randint(0, TIMES-1)
    seq.insert(r, 1)


list_insert_random_time = benchmark(lambda: random_insert(a_list))
deque_insert_random_time = benchmark(lambda: random_insert(a_deque))
gain = list_insert_random_time / deque_insert_random_time

print(f"{list_insert_random_time = :0.2f}ns")
print(f"{deque_insert_random_time = :0.2f}ns, {gain = :0.2f}x times\n")
"""
list_insert_random_time = 1177.40ns
deque_insert_random_time = 1404.90ns, gain = 0.84x times
"""

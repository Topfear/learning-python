"""
OrderedDict работает как обычный словарь, 
но в нём упорядочены значения в порядке добавления.

Преимущества:
1) OrderedDict позволяет управлять 
последовательностью элементов с помощью move_to_end().
2) При использовании OrderedDict программист 
даёт понять что последовательность важна
3) При проверке на равенство двух OrderedDict будет
иметь значение порядок элементов внутри
"""
from collections import OrderedDict

""" Три разных способа инциализировать OrderedDict """
numbers = OrderedDict({"one": 1, "two": 2, "three": 3})
person = OrderedDict(name="Jenny", lastname="Mitrofanova", age=25)
parameters = OrderedDict.fromkeys(["width", "height", "length"], 0)

print(f"{numbers = }")
for num in numbers.items():
    print(num)

""" Можно итерироваться в обратном порядке """
print(f"\nreversed {numbers = }")
for num in reversed(numbers.items()):
    print(num)


""" Перенести элемент в конец """
numbers.move_to_end("one")
# numbers = OrderedDict([('two', 2), ('three', 3), ('one', 1)])
print(f"\n{numbers = }")        

""" Перенести элемент в начало """
numbers.move_to_end("one", last=False)
# numbers = OrderedDict([('one', 1), ('two', 2), ('three', 3)])
print(f"{numbers = }")        

"""
namedtuple - неизменяемая структура данных.
Kортеж - в котором каждому элементу
соответствует именованный индекс, как в словаре.
Я бы сказал что этот тип данных похож на неизменяемый словарь.

Преимущества:
1) Получается более питонячий код. 
Сообщает программисту что тип данных неизменяемый и
каждый элемент кортежа получает своё название
2) Тип данных неизменяемый, а значит хэшируемый и его можно записать в set()
3) Доступ к элементам осуществляется по индексу и по названию элемента
"""
from collections import namedtuple


Point = namedtuple("Point", "x y", defaults=[0, 0])
point_1 = Point()               # Здесь будет значение по умолчанию 0, 0
point_2 = Point(55)             # x = 55, y = 0 (по умолчаню)
point_3 = Point(y=150)          # x = 0 (по умолчанию), y = 150
point_4 = Point(10, -10)        # x = 10, y =- 10
point_5 = Point(x=100, y=200)   # x = 100, y = 200

print(f"{point_1 = }")          # point_1 = Point(x=0, y=0)
print(f"{point_2 = }")          # point_2 = Point(x=55, y=0)
print(f"{point_3 = }")          # point_3 = Point(x=0, y=150)
print(f"{point_4 = }")          # point_4 = Point(x=10, y=-10)
print(f"{point_5 = }\n")        # point_5 = Point(x=100, y=200)

# Доступ по индексу
print(f"{point_5[0]=}")

# Доступ по имени
print(f"{point_5.x=}")
print()


""" 
Хотя кортежи и именованные кортежи неизменяемые,
у них могут быть изменяемые элементы.
Но в таком случае объект будет НЕХЭШИРУЕМЫЙ!
Такой объект не добавить в set()
"""
Person = namedtuple("Person", "name children")
ivan = Person(name="Ivan", children=["Sofia", "Bogdan"])
ivan.children.append("Stepan")  # children = ["Sofia", "Bogdan", "Stepan"]
try:
    hash(ivan)
except Exception:
    print(f"{ivan} is unhashable")

""" Нельзя создать объект без обязательных аргументов """
try:
    empty_person = Person()
except Exception as e:
    print(f"{e}\n")

ivan_as_dict = ivan._asdict()
print(ivan_as_dict)             # {'name': 'Ivan', 'children': ['Sofia', 'Bogdan', 'Stepan']}


""" namedtuple позволяет писать более питонячий код """
def custom_divmod(a, b):
    DivMod = namedtuple("DivMod", "quotient remainder")
    return DivMod(*divmod(a, b))


"""
Примечание quotient - частное, remainder - остаток
"""
result = custom_divmod(11, 2)
print(result) # DivMod(quotient=5, remainder=1)


"""
Именнованные кортежи весят меньше чем словари, примерно в 2 раза.
А также они быстрее словарей в 1.5 раза
"""
Point = namedtuple("Point", "x y z")
point = Point(1, 2, 3)

"""
Counter - класс созданный для подсчёта элементов в 
итерируемом объекте.

"""
from collections import Counter


def print_ascii_bar_chart(data, symbol="#"):
    """
    Отображение графика кол-ва элементов в списке
    """
    counter = Counter(data).most_common()
    chart = {category: symbol * frequency for category, frequency in counter}
    max_len = max(len(category) for category in chart)
    for category, frequency in chart.items():
        padding = (max_len - len(category)) * " "
        print(f"{category} : {len(frequency)}{padding} |{frequency}")
 

""" 🍌 🍊 🍑 🍒 🍈 🍉 """
grocery = [
    "🍒", "🍑", "🍒", "🍌", "🍊",
    "🍌", "🍊", "🍉", "🍌", "🍑",
    "🍑", "🍌", "🍈", "🍊", "🍌",
    "🍌", "🍈", "🍒", "🍑", "🍒",
    "🍒", "🍑", "🍒", "🍌", "🍊",
]
print_ascii_bar_chart(grocery)


""" Инициализация Counter """
grocery_counter = Counter(grocery)
print(grocery_counter)
""" Counter({'🍌': 7, '🍒': 6, '🍑': 5, '🍊': 4, '🍈': 2, '🍉': 1}) """


""" 
Обновление Counter
При обновлении значения не перезаписываются,
а добавляются к уже имеющимся 
"""
banana_grocery = ["🍌", "🍌", "🍌"]
grocery_counter.update(banana_grocery)
print(grocery_counter) # 3 банана будет добавлено


""" Если значения нет, то будет возвращён 0 """
print(f'{grocery_counter["🍎"] = }')
""" grocery_counter["🍎"] = 0 """


""" Топ-N по кол-ву """
print(f"{grocery_counter.most_common(1)}")
""" [('🍌', 10)] """
print(f"{grocery_counter.most_common(2)}")
""" [('🍌', 10), ('🍒', 6)] """
print(f"{grocery_counter.most_common(3)}")
""" [('🍌', 10), ('🍒', 6), ('🍑', 5)] """

""" Топ-3 с конца """
print(f"{grocery_counter.most_common()[:-4:-1]}")
""" [('🍉', 1), ('🍈', 2), ('🍊', 4)] """


""" Счётчики можно складывать и вычитать """
sales_day1 = Counter(apple=4, orange=9, banana=4)
sales_day2 = Counter(apple=10, orange=8, banana=6)

print(sales_day1 + sales_day2)
""" Counter({'orange': 17, 'apple': 14, 'banana': 10}) """
print(sales_day2 - sales_day1)
""" Counter({'apple': 6, 'banana': 2}) """
# Минимум продаж
print(sales_day1 & sales_day2)
""" Counter({'orange': 8, 'apple': 4, 'banana': 4}) """
# Максимум продаж
print(sales_day1 | sales_day2)
""" Counter({'apple': 10, 'orange': 9, 'banana': 6}) """
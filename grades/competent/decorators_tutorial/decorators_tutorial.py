def simple_decorator(func):
    def wrapper(*args, **kwargs):
        print('Код выполняющийся до вызова функции')
        func(*args, **kwargs)
        print('Код выполняющийся после вызова функции')
    return wrapper


@simple_decorator
def plus_5(num: int) -> int:
    print('Плюс 5')
    return num + 5


plus_5(1)
print()


def call_count(func):
    func.count = 0

    def wrapper(*args, **kwargs):
        func.count += 1
        print(f'{func.__name__} вызвана {func.count} раз!')
        func(*args, **kwargs)
    return wrapper


@call_count
def hello_world():
    print("Hello world!")


hello_world()
hello_world()
hello_world()
hello_world()
print()


def func_once(func):
    func.executed = False

    def wrapper(*args, **kwargs):
        if not func.executed:
            func(*args, **kwargs)
            func.executed = True
    return wrapper


@func_once
def hello_world():
    print("Hello world!")


hello_world()
hello_world()
hello_world()
hello_world()

"""
Изменяемые и неизменяемые типы данных
Изменяемыми типами данных называются такие типы данных,
которые при изменении значения, не изменяют свою ссылку на объект.

Неизменяемые:
1. Стандартные типы данных: bool, str, int, float
2. Таплы или кортежи: tuple
3. Замороженые сеты: frozenset

Изменяемые:
1. List
2. Dict
3. Set
4. Byte array
"""
import sys


# Проверка на изменяемость типа данных
x = "Hello world!"
y = "Hello world!"
print(x == y) # True
print(x is y) # True
# Ссылки указывают на один и тот же участок в памяти
# Т.к. при инициализации экономится память и новые переменные ссылаются на уже имеющиеся данные


x = "Hello world"
y = "Hello world!"
x += "!"
print(x) # Hello world!
print(y) # Hello world!
print(x == y) # True
print(x is y) # False
# Здесь нет проверки, поэтому ссылки разные

x = "Hello world"
x += "!"
y = "Hello world!"
print(x == y) # True
print(x is y) # False
# Видимо после изменения нет проверки для экономии памяти


# Булеаны инициализуются питоном, поэтому ссылки одинаковы
x = True
y = True
print(x == y) # True
print(x is y) # True


# Int тоже инициализируются до 2 ** 64
x = 2 ** 64 # 18446744073709551616
y = 2 ** 64 # 18446744073709551616
print(x is y) # True
print(sys.getrefcount(x)) # 5


# Int тоже инициализируются до 2 * 64
x = 2 ** 65 # 36893488147419103232
y = 2 ** 65 # 36893488147419103232
print(x is y) # False
print(sys.getrefcount(x)) # 2


# float работает так же как и str
x = 1.111
y = 1.111
print(x is y) # True

# так и есть
x = 1.0
x += 0.111
y = 1.111
print(x is y) # False


# Т.к. таплы неизменяемые совсем, 
# их ссылки всегда будут одинаковыми
x = (1, 2, 3,)
y = (1, 2, 3,)
print(x is y) # True


# Т.к. списки изменяемые типы данных, 
# их ссылки будут разными, питон на них не экономит
x = [1, 2, 3]
y = [1, 2, 3]
print(x is y) # False


# Тут очевидно будут ссылки на 
# одну и ту же область в памяти
x = [1, 2, 3]
y = x
x.append(4)
print(x) # [1, 2, 3, 4]
print(y) # [1, 2, 3, 4]
print(x is y) # True


# Ссылки будут разные, т.к. это два разных объекта
x = {1: 1, 2: 2, 3: 3}
y = {1: 1, 2: 2, 3: 3}
print(x is y) # False

# Ссылки будут разные, т.к. это два разных объекта
x = set([1, 2, 3])
y = set([1, 2, 3])
print(x is y) # False

"""
set - это множество уникальных элементов

Особенности:
1) Множество неупорядочено
2) Элементы уникальны, невозможно добавить два одинаковых элемента
3) Множество изменяемое, но его элементы должны быть неизменяемые
"""


""" Инициализация set """
numbers = set([1, 2, 3, 4, 4, 4])
letters = set("abcdefggggg")
cities = {"Moskow", "New York", "Minsk", "Cheboksary"}



""" Операции над set """
one_to_four = {1, 2, 3, 4}
three_to_six = {3, 4, 5, 6}

""" Объединение """
print(f"{one_to_four | three_to_six = }") # {1, 2, 3, 4, 5, 6}

""" Пересечение """
print(f"{one_to_four & three_to_six = }") # {3, 4}

""" Разница (порядок имеет значение) """
print(f"{one_to_four - three_to_six = }") # {1, 2}
print(f"{three_to_six - one_to_four = }") # {5, 6}

""" Симметричная разница - элементы присутствуют только в одном set """
print(f"{one_to_four ^ three_to_six = }") # {1, 2, 5, 6}


""" Является ли множество подмножеством """
one_to_two = {1, 2}
print(f"{one_to_two <= one_to_four = }")
print(f"{one_to_two <= three_to_six = }")


""" Обновление set """
one_to_four.add(5)
one_to_four.update([6, 7])
print(f"{one_to_four = }") # {1, 2, 3, 4, 5, 6, 7}

""" Удаление из set """
one_to_four.remove(7)       # вызывает исключение если не найдено
one_to_four.discard(6)      # не вызывает исключение если не найдено
one_to_four.pop()           # удаляет случайный элемент
one_to_four.clear()         # очищает весь сет
print(f"{one_to_four = }")  # {}


""" frozenset - неизменяемый set """
# неизменяемый сет можно добавить в set или как ключ словаря
one_to_two = frozenset((1, 2))
three_to_four = frozenset((3, 4))
five_to_six = frozenset((5, 6))
sets = {one_to_two, three_to_four, five_to_six}
print(sets)     # {frozenset({3, 4}), frozenset({5, 6}), frozenset({1, 2})}



""" Исследование """
class User(object):
    
    def __init__(self, name: str, inn: str):
        self.name = name
        self.inn = inn

    def __hash__(self) -> int:
        return hash(self.inn)

    def __eq__(self, other: object) -> bool:
        return (
                self.__class__ == other.__class__ and
                self.inn == other.inn
            )

# Иван
ivan = User("Ivan", "123-555-05")
# Иван сменил имя на Jake, но его инн остался прежним
jake = User("Jake", "123-555-05")

# Иван это не Jake
print(f"{ivan is jake = }")
# Но иван по сути это один и тот же человек
print(f"{ivan == jake = }")

""" Объекты с хэшем можно поместить в set           """
""" Из-за того что у Ивана и Jake одинаковые хэши   """
""" Только Иван был добавлен в set                  """
persons = {ivan, jake}
print([x.name for x in persons]) # ['Ivan']

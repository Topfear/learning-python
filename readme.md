## Python для начинающих и продвинутых разработчиков 🐍

Если у тебя завтра-послезавтра собеседование или тебе просто нужно узнать некоторые базовые аспекты языка python, ты попал по адресу.    
Здесь будет всё что нужно знать новичку или просто любопытному разработчику. 
    

### Новичок 👶

---

1. Базовые типы данных (неизменяемые/хэшируемые):
    - [Строки (```str```)](/grades/novice/basic_data_types/string_tutorial.py "Перейти к файлу")
    - [Целые числа (```int```)](/grades/novice/basic_data_types/int_tutorial.py "Перейти к файлу")
    - [Вещественные числа (```float```)](/grades/novice/basic_data_types/float_tutorial.py "Перейти к файлу")
    - [Булеан (```bool```)](/grades/novice/basic_data_types/bool_tutorial.py "Перейти к файлу")
    - [Кортежи (```tuple```)](/grades/novice/basic_data_types/tuple_tutorial.py "Перейти к файлу")
    - [Замороженные множества (```frozenset```)](/grades/novice/basic_data_types/frozenset_tutorial.py "Перейти к файлу")
2. Продвинутые типы данных (изменяемые/не хэшируемые):
    - [Списки (```list```)](/grades/novice/advanced_data_types/list_tutorial.py "Перейти к файлу")
    - [Словари (```dict```)](/grades/novice/advanced_data_types/dict_tutorial.py "Перейти к файлу")
    - [Множества (```set```)](/grades/novice/advanced_data_types/set_tutorial.py "Перейти к файлу")
    - [Последовательности байт (```bytearray```)](/grades/novice/advanced_data_types/bytearray_tutorail.py "Перейти к файлу")
3. [Чем отличаются неизменяемые типы данных от изменяемых?](/grades/novice/mutable_vs_unmutable.py "Перейти к файлу")

### Продвинутый начинающий 👦

--- 

1. Функции:
    - [Позиционные аргументы, aka ```args```](/grades/advanced_beginner/functions/positional_arguments.py "Перейти к файлу")
    - [Передача аргументов по ключу, aka ```kwargs```](/grades/advanced_beginner/functions/keyword_arguments.py "Перейти к файлу")
    - [Аргументы по умолчанию](/grades/advanced_beginner/functions/default_arguments.py "Перейти к файлу")
    - [Грамотное оформление функций](/grades/advanced_beginner/functions/functions_structure.py "Перейти к файлу")
1. Генераторы
    - [Как создать свой генератор](/grades/advanced_beginner/generators "Перейти к файлу")
2. Итераторы
    - [Способы итерирования по генератору](/grades/advanced_beginner/generators "Перейти к файлу")
4. Классы и объекты
    - Наследование и множественное наследование
    - Миксины
    - Стандартные функции в классе
    - Слоты
5. Регулярки, или ```import re```

### Компетентный 🧑‍💻

---

1. [Декораторы, с/без аргументов](/grades/competent/decorators_tutorial/decorators_tutorial.py "Перейти к файлу")
2. Контекстные менеджеры, с/без аргументов
3. Тестирование и юниттесты
4. Асинхронность, ```async``` и ```await```
4. Многопоточность и зачем она нужна
5. Многопроцессорность и зачем она нужна
6. GIL

### Умелый 😎

---

1. Типы данных из модуля ```collections```:
    - [Именованный кортеж (```namedtuple```)](/grades/skillful/collections_tutorial/namedtuple_tutorial.py "Перейти к файлу")
    - [Двусвязный список (```deque```)](/grades/skillful/collections_tutorial/deque_tutorial.py "Перейти к файлу")
    - [Словарь по умолчанию (```defaultdict```)](/grades/skillful/collections_tutorial/defaultdict_tutorial.py "Перейти к файлу")
    - [Счётчик (```Counter```)](/grades/skillful/collections_tutorial/counter_tutorial.py "Перейти к файлу")
    - [Упорядоченый словарь (```OrderedDict```)](/grades/skillful/collections_tutorial/ordereddict_tutorial.py "Перейти к файлу")
    - [Цепочка словарей (```ChainMap```)](/grades/skillful/collections_tutorial/chainmap_tutorial.py "Перейти к файлу")
2. Генераторы из ```itertools```:
    - Цикличный генератор (```cycle```)
    - Повтор n раз (```repeat```)
    - Несколько список в один (```chain``` и ```chain.from_iterable```)
    - Получение нескольких значений из генератора (```islice```)

### Эксперт 🧙‍♂️

---

Отсюда начинается ваш самостоятельный путь в мир 🐍.     
Здесь приведены лишь примеры самых популярных библиотек, не стоит учить их без необходимости.   
Изучение 3rd party библиотек:
- Django
- FastAPI
- pygame
- numpy
- python text to speech
- socket
- requests
- httpx
- aiohttp

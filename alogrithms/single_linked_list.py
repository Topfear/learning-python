# Definition for singly-linked list.
from typing import List


class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
        

class Solution(object):
    
    def min_val(self, lists):
        if not lists:
            return None
        
        min_index = 0
        min_val = None
        node = None
        
        for index, l in enumerate(lists):
            if l and (min_val or l.val < min_val):
                min_val = l.val
                min_index = index
                node = l
        
        lists[min_index] = node.next
        if not lists[min_index]:
            del lists[min_index]

        return node
    
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        node = pointer = ListNode(0)
        
        while lists:
            pointer.next = self.min_val(lists)
            pointer = pointer.next
                     
        return node.next
        

# [[1,4,5],[1,3,4],[2,6]]
a = [
    ListNode(1, next=ListNode(4, next=ListNode(5))),
    ListNode(1, next=ListNode(3, next=ListNode(4))),
    ListNode(2, next=ListNode(6)),
]

sol = Solution()
sol.mergeKLists([[]])

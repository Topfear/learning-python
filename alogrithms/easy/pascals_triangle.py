import unittest

from parameterized import parameterized


"""
Given an integer numRows, return the first numRows of Pascal's triangle.

In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:

                1
            1       1
        1       2       1
    1       3       3       1
1       4       6       4       1
"""


class Tests(unittest.TestCase):

    def generate(self, numRows: int) -> list[list[int]]:
        base = [0, 1, 0]

        result = []

        for i in range(numRows):
            new_list = [x for x in base if x != 0]
            result.append(new_list)

            new_base = []
            for idx, b in enumerate(base):
                if b == 0:
                    new_base.append(b)
                if idx == 0 or b != 0:
                    new_base.append(b + base[idx+1])
            base = new_base
        return result


    @parameterized.expand([
        (5, [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]),
        (1, [[1]]),
        (2, [[1], [1,1]]),
        (3, [[1], [1,1], [1,2,1]]),
        (4, [[1], [1,1], [1,2,1], [1,3,3,1]]),
    ])
    def test_func(self, input, expected_output):
        output = self.generate(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

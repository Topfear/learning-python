from typing import Optional
import unittest

from parameterized import parameterized


"""
Write a function to delete a node in a singly-linked list. 
You will not be given access to the head of the list, 
instead you will be given access to the node to be deleted directly.

It is guaranteed that the node to be deleted is not a tail node in the list.

"""


class ListNode:
    def __init__(self, val, next=None):
        self.val = val
        self.next = next

    def __eq__(self, o: object) -> bool:
        return self.val == o.val and self.next == o.next

    def __repr__(self) -> str:
        return str(f"{self.val} + {self.next}")


class Tests(unittest.TestCase):

    def deleteNode(self, node: ListNode) -> None:
        node.val = node.next.val
        node.next = node.next.next

    def test_func(self):
        node3 = ListNode(val=9)
        node2 = ListNode(val=1, next=node3)
        node1 = ListNode(val=5, next=node2)
        head = ListNode(val=4, next=node1)

        self.deleteNode(node1)
        self.assertEqual(head, ListNode(val=4, next=ListNode(val=1, next=ListNode(val=9))))


if __name__ == '__main__':
    unittest.main()

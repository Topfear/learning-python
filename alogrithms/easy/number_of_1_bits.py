import unittest

from parameterized import parameterized


"""
Write a function that takes an unsigned integer and returns 
the number of '1' bits it has (also known as the Hamming weight).

"""


class Tests(unittest.TestCase):

    def hammingWeight(self, n: int) -> int:
        # not optimal
        if False:
            bit_str = bin(n)
            
            count = 0
            for bit_ch in bit_str:
                if bit_ch == '1':
                    count += 1
            return count

        # optimal
        else:
            count = 0
            
            while True:
                if n & 0b0001:
                    count += 1
                elif n == 0b0:
                    return count
                
                n = n >> 1

    @parameterized.expand([
        (11, 3),
        (1024, 1),
        (15, 4),
    ])
    def test_func(self, input, expected_output):
        output = self.hammingWeight(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

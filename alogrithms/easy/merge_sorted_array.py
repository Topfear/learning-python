import unittest

from parameterized import parameterized


"""
You are given two integer arrays nums1 and nums2, sorted in non-decreasing order, 
and two integers m and n, representing the number of elements in nums1 and nums2 respectively.

Merge nums1 and nums2 into a single array sorted in non-decreasing order.

The final sorted array should not be returned by the function, 
but instead be stored inside the array nums1. To accommodate this, 
nums1 has a length of m + n, where the first m elements denote 
the elements that should be merged, and the last n elements are set to 
0 and should be ignored. nums2 has a length of n.

"""


class Tests(unittest.TestCase):

    def merge(self, nums1: list[int], m: int, nums2: list[int], n: int) -> None:
        p_1 = m - 1
        p_2 = n - 1
        p_3 = n + m - 1

        while p_1 >= 0 and p_2 >= 0:
            if nums2[p_2] > nums1[p_1]:
                nums1[p_3] = nums2[p_2]
                p_3 -= 1
                p_2 -= 1
            else:
                nums1[p_3], nums1[p_1] = nums1[p_1], nums1[p_3]
                p_3 -= 1
                p_1 -= 1
            
        while p_2 >= 0:
            nums1[p_3] = nums2[p_2]
            p_3 -= 1
            p_2 -= 1


    @parameterized.expand([
        ([1,2,3,0,0,0], 3, [2,5,6], 3, [1,2,2,3,5,6]),
        ([5,6,7,0,0,0], 3, [1,2,3], 3, [1,2,3,5,6,7]),
        ([1,3,5,0,0,0], 3, [2,4,6], 3, [1,2,3,4,5,6]),
        ([-300,-200,-100,0,0,0], 3, [-600,-500,-400], 3, [-600,-500,-400,-300,-200,-100]),
        ([1], 1, [], 0, [1]),
        ([0], 0, [1], 1, [1]),
    ])
    def test_func(self, nums1, m, nums2, n, expected_output):
        self.merge(nums1=nums1, m=m, nums2=nums2, n=n)
        self.assertEqual(nums1, expected_output)


if __name__ == '__main__':
    unittest.main()

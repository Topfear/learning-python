from typing import Optional
import unittest

from parameterized import parameterized


"""
Given the root of a binary tree, return the inorder traversal of its nodes' values.

        1
    2       3
4      5  6     7

output = [4, 2, 5, 1, 6, 3, 7]

"""

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Tests(unittest.TestCase):

    def recursive_inorder_traversal(self, root: TreeNode, res: list) -> list:
        if not root:
            return None
        else:
            self.recursive_inorder_traversal(root.left, res)
            res.append(root.val)
            self.recursive_inorder_traversal(root.right, res)


    def inorderTraversal(self, root: Optional[TreeNode]) -> list[int]:
        solution = 2
        # Solution 1
        if solution == 1:
            if not root:
                return []
            
            return self.inorderTraversal(root.left) + [root.val] + self.inorderTraversal(root.right)

        # Solution 2
        if solution == 2:
            res = []
            self.recursive_inorder_traversal(root, res)
            return res

    @parameterized.expand([
        (TreeNode(1, right=TreeNode(2, left=TreeNode(3))), [1,3,2]),
        (None, []),
        (TreeNode(1), [1]),
    ])
    def test_func(self, input, expected_output):
        output = self.inorderTraversal(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

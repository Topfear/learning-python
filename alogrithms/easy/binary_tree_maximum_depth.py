from typing import Optional
import unittest

from parameterized import parameterized


"""
Given the root of a binary tree, return its maximum depth.

A binary tree's maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

        1
    2       2
       4       

output = 3

            1
        2         2
    8     9   8     9
10

output = 4
"""

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Tests(unittest.TestCase):

    def maxDepth(self, root: Optional[TreeNode]) -> int:
        if not root:
            return 0
        return max(self.maxDepth(root.left), self.maxDepth(root.right)) + 1
        
    @parameterized.expand([
        (
            TreeNode(1, 
                left=TreeNode(2, 
                    left=TreeNode(3), 
                    right=TreeNode(4)), 
                right=TreeNode(2, 
                    left=TreeNode(4), 
                    right=TreeNode(3))), 
            3),
        (
            TreeNode(1, 
                left=TreeNode(2, 
                    left=TreeNode(3), 
                    right=TreeNode(4)), 
                right=TreeNode(2, 
                    left=TreeNode(4), 
                    right=TreeNode(3, 
                        left=TreeNode(10)))), 
            4),
        (
            TreeNode(1, 
                left=TreeNode(2, 
                    left=TreeNode(3), 
                    right=TreeNode(4)), 
                right=TreeNode(2, 
                    left=TreeNode(4), 
                    right=TreeNode(100))), 3),
        (None, 0),
        (TreeNode(1), 1),
        (TreeNode(1, left=TreeNode(0)), 2),
    ])
    def test_func(self, input, expected_output):
        output = self.maxDepth(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

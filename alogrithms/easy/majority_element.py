from collections import defaultdict
import unittest

from parameterized import parameterized


"""
Given an array nums of size n, return the majority element.

The majority element is the element that appears more than ⌊n / 2⌋ times. 
You may assume that the majority element always exists in the array.

"""


class Tests(unittest.TestCase):

    def majorityElement(self, nums: list[int]) -> int:
        optimal = True

        # not optimal
        if not optimal:
            memo = defaultdict(int)
            limit = len(nums) // 2

            for num in nums:
                memo[num] += 1
                if memo[num] > limit:
                    return num
        # optimal
        else:
            counter = 0
            major = None

            for num in nums:
                if counter == 0:
                    major = num
                if num == major:
                    counter += 1
                else:
                    counter -= 1
            return major

    @parameterized.expand([
        ([3,2,3], 3),
        ([2,2,1,1,1,2,2], 2),
        ([1,2,3,4,5,5,5,5,5], 5),
        ([4,4,4,4,5,5,5,5,5], 5),
        ([5,5,5,5,5,1,2,3,4], 5),
    ])
    def test_func(self, input, expected_output):
        output = self.majorityElement(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

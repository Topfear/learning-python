from typing import Optional
import unittest

from parameterized import parameterized


"""
You are given the heads of two sorted linked lists list1 and list2.

Merge the two lists in a one sorted list. 
The list should be made by splicing together the nodes of the first two lists.

Return the head of the merged linked list.

"""

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    def __eq__(self, o: object) -> bool:
        return self.val == o.val and self.next == o.next

    def __repr__(self) -> str:
        return str(f"{self.val} + {self.next}")


class Solution(unittest.TestCase):

    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        result = ListNode()
        result_pointer = result

        while list1 and list2:
            if list1.val < list2.val:
                result_pointer.next = list1
                list1 = list1.next
            else:
                result_pointer.next = list2
                list2 = list2.next
            result_pointer = result_pointer.next

        result_pointer.next = list1 or list2

        return result.next
    
    @parameterized.expand([
        (
            (ListNode(1, ListNode(2, ListNode(4)))), 
            (ListNode(1, ListNode(3, ListNode(4)))), 
            (ListNode(1, ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(4)))))))),
        (
            (ListNode(1, ListNode(3, ListNode(5)))), 
            (ListNode(2, ListNode(4, ListNode(6)))), 
            (ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5, ListNode(6)))))))),
        (None, None, None),
        (None, ListNode(0), ListNode(0)),
        (ListNode(0), None, ListNode(0)),
    ])
    def test_func(self, list1, list2, expected_output):
        output = self.mergeTwoLists(list1, list2)
        self.assertEqual(output, expected_output)

if __name__ == '__main__':
    unittest.main()

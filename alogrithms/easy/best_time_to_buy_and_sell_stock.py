import unittest

from parameterized import parameterized


"""
You are given an array prices where prices[i] is the 
price of a given stock on the ith day.

You want to maximize your profit by choosing a single day to buy 
one stock and choosing a different day in the future to sell that stock.

Return the maximum profit you can achieve from this transaction. 
If you cannot achieve any profit, return 0.

price:      4 3 2 5 6 7 1 9
min_price:  4 3 2 2 2 2 1 1
profit:     0 0 0 3 4 5 0 8

"""


class Tests(unittest.TestCase):

    def maxProfit(self, prices: list[int]) -> int:
        max_profit, min_price = 0, float('inf')
        for price in prices:
            min_price = min(min_price, price)
            profit = price - min_price
            max_profit = max(max_profit, profit)
        return max_profit

    @parameterized.expand([
        ([7,1,5,3,6,4], 5),
        ([7,6,4,3,1], 0),
        ([1,2,3,4,5], 4),
        ([], 0),
        ([5, 4, 3, 2, 1], 0),
        ([2,4,1], 2),
    ])  
    def test_func(self, input, expected_output):
        output = self.maxProfit(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

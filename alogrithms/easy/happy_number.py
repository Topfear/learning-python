import unittest

from parameterized import parameterized


"""
Write an algorithm to determine if a number n is happy.

A happy number is a number defined by the following process:

Starting with any positive integer, replace the number by the sum of the squares of its digits.
Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.
Those numbers for which this process ends in 1 are happy.
Return true if n is a happy number, and false if not.
"""


class Tests(unittest.TestCase):

    def isHappy(self, n: int) -> bool:
        
        def get_next(n: int) -> int:
            total_sum = 0
            while n > 0:
                n, digit = divmod(n, 10)
                total_sum += digit ** 2
            return total_sum

        hash_set = set()
        while n not in hash_set and n != 1:
            hash_set.add(n)
            n = get_next(n)

        return n == 1

    @parameterized.expand([
        (19, True),
        (2, False),
        (7, True),
        (116, False),
    ])
    def test_func(self, input, expected_output):
        output = self.isHappy(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

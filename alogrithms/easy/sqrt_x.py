import unittest

from parameterized import parameterized


"""
Given a non-negative integer x, compute and return the square root of x.

Since the return type is an integer, the decimal digits are truncated, and only the integer part of the result is returned.

Note: You are not allowed to use any built-in exponent function or operator, such as pow(x, 0.5) or x ** 0.5.

"""


class Tests(unittest.TestCase):

    def mySqrt(self, x: int) -> int:
        left = 0
        right = x

        while left <= right:
            center = (right + left) // 2

            if center * center <= x < (center + 1) * (center + 1):
                return center
            elif center * center < x:
                left = center + 1
            else:
                right = center - 1

    @parameterized.expand([
        (4, 2),
        (8, 2),
        (9, 3),
        (1024, 32),
        (1_000_000, 1_000),
        (1_000_000_000_000, 1_000_000),
    ])
    def test_func(self, input, expected_output):
        output = self.mySqrt(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

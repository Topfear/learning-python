import unittest

from parameterized import parameterized


"""
Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', 
determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
"""


class Tests(unittest.TestCase):

    def isValid(self, s: str) -> bool:
        open_brackets = ['(', '{', '[']
        brackets_comparison = {')': '(', '}': '{', ']': '['}
        stack = []
        
        for char in s:
            if char in open_brackets:
                stack.append(char)
            else:
                if not stack or stack.pop() != brackets_comparison[char]:
                    return False

        return True if not stack else False

    @parameterized.expand([
        (r'()[]{}', True),
        (r'', True),
        (r'()', True),
        (r"{}", True),
        (r'({})', True),
        (r'[({})]', True),
        (r'[({})]]', False),
        (r'][({})]]', False),
        (r'[[({]})]', False),
        (r'[[({]})]', False),
        (r'[({]})]', False),
        (r'[', False),
    ])
    def test_func(self, input, output):
        input = self.isValid(input)
        self.assertEqual(input, output)


if __name__ == '__main__':
    unittest.main()

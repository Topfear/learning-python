import string
import unittest

from parameterized import parameterized


"""
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

"""


class Tests(unittest.TestCase):

    def isPalindrome(self, s: str) -> bool:
        s = s.lower()
        letters_and_digits = string.ascii_lowercase + string.digits
        s = list(filter(lambda x: x in letters_and_digits, s))
        
        for i in range(len(s) // 2):
            if s[i] != s[len(s) - 1 - i]:
                return False
        return True


    @parameterized.expand([
        ("abba", True),
        ("ab ba", True),
        ("kazak", True),
        ("kazan", False),
        ("kazan: aa, nazak", True),
        ("kazan: is, nazak", False),
        ("kazan wtf", False),
    ])
    def test_func(self, input, expected_output):
        output = self.isPalindrome(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

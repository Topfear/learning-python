from typing import Optional
import unittest

from parameterized import parameterized


"""
Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).

        1
    2       2
3      4  4     3

output = True

        1
   2         2
8     9   8     9

"""

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Tests(unittest.TestCase):

    def isSymmetric(self, root: Optional[TreeNode]) -> bool:
        if not root:
            return True
        
        stack = [root]

        while stack:
            next_stack = []
            line = []

            for curr in stack:
                if curr.left:
                    next_stack.append(curr.left)
                if curr.right:
                    next_stack.append(curr.right)
                line.append(curr.left.val if curr.left else None)
                line.append(curr.right.val if curr.right else None)
            stack = next_stack
            
            line_len = len(line)
            for i in range(line_len // 2):
                if line[i] != line[line_len - i - 1]:
                    return False
        
        return True

    @parameterized.expand([
        (
            TreeNode(1, 
                left=TreeNode(2, 
                    left=TreeNode(3), 
                    right=TreeNode(4)), 
                right=TreeNode(2, 
                    left=TreeNode(4), 
                    right=TreeNode(3))), 
            True),
        (
            TreeNode(1, 
                left=TreeNode(2, 
                    left=TreeNode(3), 
                    right=TreeNode(4)), 
                right=TreeNode(2, 
                    left=TreeNode(4), 
                    right=TreeNode(100))), False),
        (None, True),
        (TreeNode(1), True),
        (TreeNode(1, left=TreeNode(0)), False),
    ])
    def test_func(self, input, expected_output):
        output = self.isSymmetric(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

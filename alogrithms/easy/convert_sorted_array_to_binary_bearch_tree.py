from typing import Optional
import unittest

from parameterized import parameterized


"""
Given an integer array nums where the elements are sorted in 
ascending order, convert it to a height-balanced binary search tree.

A height-balanced binary tree is a binary tree in which the depth 
of the two subtrees of every node never differs by more than one.

"""


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def __eq__(self, o: object) -> bool:
        return self.val == o.val and self.left == o.left and self.right == o.right

    def __repr__(self) -> str:
        return str(f"{self.val} + {self.left} + {self.right}")


class Tests(unittest.TestCase):

    def sortedArrayToBST(self, nums: list[int]) -> Optional[TreeNode]:
        if not nums:
            return None
        
        middle = len(nums) // 2
        root = TreeNode(nums[middle])
        root.left = self.sortedArrayToBST(nums[:middle])
        root.right = self.sortedArrayToBST(nums[middle+1:])
        return root

    @parameterized.expand([
        ([-10,-3,0,5,9], 
        TreeNode(val=0, 
            left=TreeNode(val=-3, 
                left=TreeNode(val=-10)), 
            right=TreeNode(val=9, 
                left=TreeNode(val=5)))),
        ([1, 3], TreeNode(val=3, left=TreeNode(1))),
    ])
    def test_func(self, input, expected_output):
        output = self.sortedArrayToBST(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

import unittest

from parameterized import parameterized


"""
Implement strStr().

Given two strings needle and haystack, return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

Clarification:

What should we return when needle is an empty string? This is a great question to ask during an interview.

For the purpose of this problem, we will return 0 when needle is an empty string. This is consistent to C's strstr() and Java's indexOf().

"""


class Tests(unittest.TestCase):

    def strStr(self, haystack: str, needle: str) -> int:
        """
        Поиск подстроки в строке, лучший способ на питоне:
        return haystack.find(needle)
        мною представлен O(N*M) вариант решения задачи
        """

        if not needle:
            return 0

        first_occur_pointer = -1
        needle_pointer = 0
        
        haystach_sub_needle = len(haystack) - len(needle)

        for idx in range(haystach_sub_needle + 1):
            for jdx in range(len(needle)):
                if haystack[idx + jdx] == needle[jdx]:
                    if first_occur_pointer == -1:
                        first_occur_pointer = idx
                    needle_pointer += 1
                    if needle_pointer == len(needle):
                        return first_occur_pointer
                else:
                    first_occur_pointer = -1
                    needle_pointer = 0
                    break

        return -1

    @parameterized.expand([
        ("hello", "llqqqqqqqqqqqqqqqqqq", -1),
        ("hello", "ll", 2),
        ("hello", "", 0),
        ("aaaaaaaaaaaaaa", "vv", -1),
        ("abcabcd", "abcd", 3),
        ("a", "a", 0),
        ("abc", "c", 2),
    ])  
    def test_func(self, haystack, needle, expected):
        output = self.strStr(haystack, needle)
        self.assertEqual(output, expected)


if __name__ == '__main__':
    unittest.main()

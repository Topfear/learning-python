import unittest

from parameterized import parameterized


"""
Given an integer n, return a string array answer (1-indexed) where:

answer[i] == "FizzBuzz" if i is divisible by 3 and 5.
answer[i] == "Fizz" if i is divisible by 3.
answer[i] == "Buzz" if i is divisible by 5.
answer[i] == i (as a string) if none of the above conditions are true.

"""


class Tests(unittest.TestCase):

    def fizzBuzz(self, n: int) -> list[str]:
        result = []

        for i in range(1, n + 1):
            fizz = "Fizz" if not(i % 3) else ""
            buzz = "Buzz" if not(i % 5) else ""
            fizz_buzz = fizz + buzz

            if fizz_buzz:
                result.append(fizz_buzz)
            else:
                result.append(str(i))
        return result

    @parameterized.expand([
        (3, ["1","2","Fizz"]),
        (5, ["1","2","Fizz","4","Buzz"]),
        (15, ["1","2","Fizz","4","Buzz","Fizz","7","8","Fizz","Buzz","11","Fizz","13","14","FizzBuzz"]),
    ])
    def test_func(self, input, expected_output):
        output = self.fizzBuzz(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

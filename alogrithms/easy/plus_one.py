import unittest

from parameterized import parameterized


"""
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

"""


class Tests(unittest.TestCase):

    def plusOne(self, digits: list[int]) -> list[int]:
        increment = 1
        for idx in range(len(digits) - 1, -1, -1):
            digits[idx] += increment

            if digits[idx] == 10:
                digits[idx] = 0
            else:
                increment = 0
                break
        
        if increment:
            digits.insert(0, 1)
        return digits


    @parameterized.expand([
        ([1, 2, 3], [1, 2, 4]),
        ([1, 2, 3, 5, 6], [1, 2, 3, 5, 7]),
        ([9], [1, 0]),
        ([1], [2]),
        ([9, 9, 9], [1, 0, 0, 0]),
    ])
    def test_func(self, input, expected_output):
        output = self.plusOne(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

from inspect import stack
import unittest

from parameterized import parameterized


"""
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

"""

class MinStack:

    def __init__(self):
        self.stack = []
        self.min_stack = []

    def push(self, val: int) -> None:
        self.stack.append(val)

        if not self.min_stack or val <= self.min_stack[-1]:
            self.min_stack.append(val)

    def pop(self) -> None:
        pop_val = self.stack.pop()

        if pop_val == self.min_stack[-1]:
            self.min_stack.pop()

    def top(self) -> int:
        return self.stack[-1]

    def getMin(self) -> int:
        return self.min_stack[-1]
        

class Tests(unittest.TestCase):

    def test_func(self):
        min_stack = MinStack()
        min_stack.push(-2)
        min_stack.push(0)
        min_stack.push(-3)
        output = min_stack.getMin()
        self.assertEqual(output, -3)
        min_stack.pop()
        output = min_stack.top()
        self.assertEqual(output, 0)
        output = min_stack.getMin()
        self.assertEqual(output, -2)


if __name__ == '__main__':
    unittest.main()

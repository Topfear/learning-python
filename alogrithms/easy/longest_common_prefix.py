import unittest

from parameterized import parameterized

"""
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

"""

class Solution(unittest.TestCase):

    def longestCommonPrefix(self, strs: list[str]) -> str:
        zip_strs = zip(*strs)
        result = ""

        for zip_str in zip_strs:
            char = zip_str[0]

            if all(x == char for x in zip_str):
                result += char
            else:
                break

        return result

    @parameterized.expand([
        (["flower","flow","flight"], "fl"),
        (["flower","","flight"], ""),
        (["dog","racecar","car"], ""),
        (["car","car","car"], "car"),
        (["car","carqqqqqq","car"], "car"),
    ])
    def test_func(self, input, output):
        input = self.longestCommonPrefix(input)
        self.assertEqual(input, output)


if __name__ == '__main__':
    unittest.main()

import unittest

from parameterized import parameterized


"""
An array is monotonic if it is either monotone increasing or monotone decreasing.

An array nums is monotone increasing if for all i <= j, nums[i] <= nums[j]. An array nums is monotone decreasing if for all i <= j, nums[i] >= nums[j].

Given an integer array nums, return true if the given array is monotonic, or false otherwise.

"""


class Tests(unittest.TestCase):

    def isMonotonic(self, nums: list[int]) -> bool:
        is_increasing = True
        is_decreasing = True

        for i in range(len(nums) - 1):
            if nums[i] < nums[i + 1]:
                is_decreasing = False
            if nums[i] > nums[i + 1]:
                is_increasing = False
                
        return is_decreasing or is_increasing

    @parameterized.expand([
        ([1, 2, 3, 4, 5], True),
        ([5, 4, 3, 2, 1], True),
        ([0, 0, 0, 0, 0], True),
        ([0, 1, 1, 1, 2, 2, 3, 3, 3], True),
        ([0, 1, 1, 1, 1, 1, 0, 0, 0], False),
        ([0, 1, 2, 3, 4, 3, 2, 1, 0], False),
        ([0, 1, 2, 3, 3, 3, 3, 2, 1], False),
    ])
    def test_func(self, input, expected_output):
        output = self.isMonotonic(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

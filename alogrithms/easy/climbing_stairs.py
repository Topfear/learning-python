import unittest

from parameterized import parameterized


"""
You are climbing a staircase. It takes n steps to reach the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

"""


class Tests(unittest.TestCase):

    def climbStairs(self, n: int) -> int:
        memo = {
            1: 1,
            2: 2,
            3: 3,
        }
        if n in memo: return memo[n]

        for idx in range(4, n + 1):
            memo[idx] = memo[idx - 1] + memo[idx - 2]
        return memo[n]

    @parameterized.expand([
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 5),
        (5, 8),
        (6, 13),
    ])
    def test_func(self, input, expected_output):
        output = self.climbStairs(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

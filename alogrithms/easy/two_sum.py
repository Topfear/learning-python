import unittest

from parameterized import parameterized


"""
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

"""


class Tests(unittest.TestCase):

    def twoSum(self, nums: list[int], target: int) -> list[int]:
        data = {}
        for index, num in enumerate(nums):
            remainder = target - num

            if remainder in data:
                return [data[remainder], index]

            data[num] = index

    @parameterized.expand([
        (([2, 7, 11, 15], 9), [0, 1]),
        (([3, 3], 6), [0, 1]),
        (([3, 2, 4], 6), [1, 2]),
        (([1, 2, 3, 4, 5, 6, 7], 13), [5,6]),
    ])
    def test_func(self, input, output):
        input = self.twoSum(*input)
        self.assertEqual(input, output)


if __name__ == '__main__':
    unittest.main()

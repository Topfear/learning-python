from cgitb import reset
import unittest

from parameterized import parameterized


"""
Given a string columnTitle that represents the column title as appears in an Excel sheet, return its corresponding column number.

For example:

A -> 1
B -> 2
C -> 3
...
Z -> 26
AA -> 27
AB -> 28 
"""


class Tests(unittest.TestCase):

    def titleToNumber(self, columnTitle: str) -> int:
        def ch_to_int(ch: str):
            return ord(ch) - ord('A') + 1
        
        mul = 1
        result = 0
        for ch in range(len(columnTitle)-1, -1, -1):
            result += mul * ch_to_int(columnTitle[ch])
            mul *= 26
        return result

    @parameterized.expand([
        ("A", 1),
        ("Z", 26),
        ("AB", 28),
        ("ZY", 701),
    ])
    def test_func(self, input, expected_output):
        output = self.titleToNumber(input)
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()

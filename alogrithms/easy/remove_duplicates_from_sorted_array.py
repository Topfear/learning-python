import unittest

from parameterized import parameterized

"""
Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place such that each unique element appears only once. The relative order of the elements should be kept the same.

Since it is impossible to change the length of the array in some languages, you must instead have the result be placed in the first part of the array nums. More formally, if there are k elements after removing the duplicates, then the first k elements of nums should hold the final result. It does not matter what you leave beyond the first k elements.

Return k after placing the final result in the first k slots of nums.

Do not allocate extra space for another array. You must do this by modifying the input array in-place with O(1) extra memory.

"""


class Tests(unittest.TestCase):

    def removeDuplicates(self, nums: list[int]) -> int:
        if not nums:
            return 0

        pointer = 0
        prev_num = None

        for idx, num in enumerate(nums):
            if num != prev_num:
                nums[pointer] = num
                pointer += 1
            prev_num = num

        return pointer
                

    @parameterized.expand([
        ([1, 2, 2], [1, 2], 2),
        ([0,0,1,1,1,2,2,3,3,4], [0,1,2,3,4], 5),
        ([], [], 0),
    ])
    def test_func(self, input, output, output_len):
        input_len = self.removeDuplicates(input)
        self.assertEqual(input_len, output_len)

        for i in range(output_len):
            self.assertEqual(input[i], output[i])

if __name__ == '__main__':
    unittest.main()

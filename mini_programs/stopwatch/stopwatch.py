import time
from decimal import Decimal
from threading import Timer  

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QThread


class RepeatTimer(QThread, Timer):  
    counter = 0
    running = False
    _start = 0

    def run(self):  
        while self.running and not self.finished.wait(self.interval):  
            self.function(*self.args,**self.kwargs)

    def stop(self):
        self.running = False
        self.counter = 0
        self._start = 0

    def pause(self):
        finish = time.time()
        self.counter = self.counter + finish - self._start
        self._start = 0
        self.running = False

    def unpause(self):
        self._start = time.time()
        self.running = True

    def get_ms(self):
        if not self._start and self.counter:
            mseconds = self.counter
        else:
            mseconds = self.counter + (time.time() - self._start)

        mseconds = round(mseconds, 2)
        return mseconds * 100


class Ui_QMainWindow(object):
    default_time = "00:00:00"
    time = default_time
    active = False

    def __init__(self):
        self.timer = RepeatTimer(interval=0.01, function=self.refresh_timer)

    def setupUi(self, QMainWindow):
        QMainWindow.setObjectName("QMainWindow")
        QMainWindow.setEnabled(True)
        QMainWindow.resize(300, 150)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(QMainWindow.sizePolicy().hasHeightForWidth())
        QMainWindow.setSizePolicy(sizePolicy)
        QMainWindow.setMinimumSize(QtCore.QSize(300, 150))
        QMainWindow.setMaximumSize(QtCore.QSize(300, 150))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("D:/Projects/learning python/mini programs/stopwatch/timer.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        QMainWindow.setWindowIcon(icon)
        QMainWindow.setAccessibleName("")
        self.centralwidget = QtWidgets.QWidget(QMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(0, 10, 300, 30))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setStyleSheet("")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(100, 60, 100, 30))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.pushButton.setFont(font)
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(100, 100, 100, 30))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.pushButton_2.setFont(font)
        self.pushButton_2.setObjectName("pushButton_2")
        QMainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(QMainWindow)
        self.connect_buttons()
        QtCore.QMetaObject.connectSlotsByName(QMainWindow)

    def retranslateUi(self, QMainWindow):
        _translate = QtCore.QCoreApplication.translate
        QMainWindow.setWindowTitle(_translate("QMainWindow", "Секундомер"))
        self.label.setText(_translate("QMainWindow", self.default_time))
        self.pushButton.setText(_translate("QMainWindow", "Старт"))
        self.pushButton_2.setText(_translate("QMainWindow", "Стоп"))

    def connect_buttons(self):
        self.pushButton.clicked.connect(self.start_pause)
        self.pushButton_2.clicked.connect(self.stop)

    def refresh_timer(self):
        total_ms = Decimal(self.timer.get_ms()).quantize(1)
        ms = total_ms % 100
        total_ms = total_ms // 100
        seconds = total_ms % 60
        total_ms = total_ms // 60
        minutes = total_ms
        self.label.setText(f"{minutes:02}:{seconds:02}:{ms:02}")

    def start_pause(self):
        self.active = not self.active
        
        if self.active:
            self.pushButton.setText("Пауза")
            self.timer.unpause()
            self.timer.start()
        else:
            self.pushButton.setText("Старт")
            self.timer.pause()

    def stop(self):
        self.timer.stop()
        time.sleep(0.02)
        self.pushButton.setText("Старт")
        self.label.setText(self.default_time)
        self.active = False


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    QMainWindow = QtWidgets.QMainWindow()
    ui = Ui_QMainWindow()
    ui.setupUi(QMainWindow)
    QMainWindow.show()
    sys.exit(app.exec_())

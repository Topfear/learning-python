import requests
from time import time
import aiohttp
import asyncio


apis = [
    "http://numbersapi.com/random/trivia",
    "http://numbersapi.com/random/year",
    "http://numbersapi.com/random/date",
    "http://numbersapi.com/random/math",
    "https://yesno.wtf/api",
    "http://jservice.io/api/random?count=1",
    "https://randomfox.ca/floof/",
    "https://random.dog/woof.json",
    "https://aws.random.cat/meow",
    "https://rickandmortyapi.com/api/character/1",
]


def make_sync_api_call():
    for api in apis:
        start = time()
        response = requests.get(api)
        end = time()
        print(f'{response.text[:20]}  {(end - start) * 1000:.0f}ms')


async def fetch(session, url):
    async with session.get(url) as response:
        text = await response.text()
        return text


async def fetch_all(urls):
    async with aiohttp.ClientSession() as session:
        start = time()
        responses = await asyncio.gather(*[fetch(session, url) for url in urls], return_exceptions=True)
        end = time()
        for r in responses:
            print(f'{r[:20]}  {(end - start) * 1000:.0f}ms')
        return responses
        

def make_aiohtpp_api_call():
    loop = asyncio.get_event_loop()
    responses = loop.run_until_complete(fetch_all(apis))


def main():
    """
    Пакет осуществляет 10 запросов на 10 разных API
    Способы осуществления запросов:
    1. Синхронный
    2. aihttp
    3. async|await
    4. Потоки (Thread)
    5. Процессы (multiprocessing)
    """
    start = time()
    make_sync_api_call()
    end = time()
    print(f"Sync api requests: {(end-start) * 1000:.0f}ms")

    print('-------------\n' * 5)

    start = time()
    make_aiohtpp_api_call()
    end = time()
    print(f"Aiohttp api requests: {(end-start) * 1000}ms")




if __name__ == "__main__":
    main()
